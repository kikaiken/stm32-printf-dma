//
// Created by Akitomo Kuraku on 25/02/07.
//

#include "main.h"
#include "dma_ring.h"
#include "dma_rx.h"
#include <stdio.h> // for debug

void dma_rx_init(struct dma_rx_info *rx, UART_HandleTypeDef *rx_huart){
	rx->huart = rx_huart;
	dma_ring_init(&rx->rx_ring, rx_huart);
	HAL_UART_Receive_DMA(rx->huart, rx->rx_ring.buf, rx->rx_ring.buf_size);
}

uint8_t dma_rx_getc_blocking(struct dma_rx_info *rx){
	// while(dma_ring_available(&rx->rx_ring) == 0){
	// 	dma_ring_set_w_ptr(&rx->rx_ring, (uint16_t)((rx->rx_ring.buf_size - __HAL_DMA_GET_COUNTER(rx->huart->hdmarx))&0xFFFF));
	// }
	while(dma_ring_available(&rx->rx_ring) == 0){
		;
		// printf("wait\n"); // debug
	}
	uint8_t c;
	if(dma_ring_getc(&rx->rx_ring, &c) == RING_FAIL){
		// printf("[dma_rx_getc_blocking()] RING_FAIL\n"); // debug
	}
	// printf("[dma_rx_getc_blocking()] %02x\n", c); // debug
	return c;
}

void dma_rx_gets_blocking(struct dma_rx_info *rx, uint8_t *buf, size_t len){
	for(size_t i=0; i<len; i++){
		buf[i] = dma_rx_getc_blocking(rx);
	}
}

void dma_rx_clear(struct dma_rx_info *rx){
	while(dma_ring_available(&rx->rx_ring) > 0){
		(void)dma_rx_getc_blocking(rx);
	}
}
