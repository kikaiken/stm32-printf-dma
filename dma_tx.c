//
// Created by Akitomo Kuraku on 25/02/07.
//

#include "main.h"
#include "dma_tx.h"
#include "dma_ring.h"
#include <stdbool.h>

void dma_tx_init(struct dma_tx_info *tx, UART_HandleTypeDef *tx_huart){
	tx->huart = tx_huart;
	dma_ring_init(&tx->tx_ring, tx_huart);
	tx->sending = false;
	tx->previous_send_len = 0;
}

void dma_tx_putc(struct dma_tx_info *tx, uint8_t c){
	dma_ring_putc(&tx->tx_ring, c);
	if(!tx->sending){
		uint16_t len = (uint16_t)dma_ring_available_linear(&tx->tx_ring);
		tx->previous_send_len = len;
		tx->sending = true;
		HAL_UART_Transmit_DMA(tx->huart, tx->tx_ring.buf + dma_ring_get_r_ptr(&tx->tx_ring), len);
	}
}

void dma_tx_puts(struct dma_tx_info *tx, uint8_t *data, size_t data_len){
	for(size_t i=0; i<data_len; i++){
		dma_ring_putc(&tx->tx_ring, data[i]);
	}
	if(!tx->sending){
		uint16_t len = (uint16_t)dma_ring_available_linear(&tx->tx_ring);
		tx->previous_send_len = len;
		tx->sending = true;
		HAL_UART_Transmit_DMA(tx->huart, tx->tx_ring.buf + dma_ring_get_r_ptr(&tx->tx_ring), len);
	}
}

void dma_tx_send_it(struct dma_tx_info *tx, UART_HandleTypeDef *tx_huart) {
	if(tx->huart != tx_huart) return;

	dma_ring_forward_r_ptr(&tx->tx_ring, tx->previous_send_len);
	uint16_t len = (uint16_t)dma_ring_available_linear(&tx->tx_ring);
	if(len > 0){
		tx->previous_send_len = len;
		tx->sending = true;
		HAL_UART_Transmit_DMA(tx->huart, tx->tx_ring.buf + dma_ring_get_r_ptr(&tx->tx_ring), len);
	}else{
		tx->sending = false;
	}
}

void dma_tx_flush(struct dma_tx_info *tx){
	while(tx->sending){
		;
	}
}
