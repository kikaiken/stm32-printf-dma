//
// Created by Akitomo Kuraku on 25/02/07.
//

#ifndef HAL_DMA_PRINTF_DMA_RX_H
#define HAL_DMA_PRINTF_DMA_RX_H
#include "dma_ring.h"
#include "main.h"

struct dma_rx_info {
	struct dma_ring_buf rx_ring;
	UART_HandleTypeDef *huart;
};

void dma_rx_init(struct dma_rx_info *rx, UART_HandleTypeDef *rx_huart);
uint8_t dma_rx_getc_blocking(struct dma_rx_info *rx);
void dma_rx_gets_blocking(struct dma_rx_info *rx, uint8_t *buf, size_t len);
void dma_rx_clear(struct dma_rx_info *rx);

#endif //HAL_DMA_PRINTF_DMA_RX_H
