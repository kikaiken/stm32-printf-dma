//
// Created by Akitomo Kuraku on 25/02/13.
//

#include "main.h"
#include "dma_serial.h"
#include "dma_ring.h"
#include "dma_tx.h"
#include <stdio.h> // for debug

void dma_serial_init(struct dma_serial *serial, UART_HandleTypeDef *huart){
	serial->huart = huart;
	dma_tx_init(&serial->tx_info, serial->huart);
	dma_ring_init(&serial->rx_ring, serial->huart);
	HAL_UART_Receive_DMA(serial->huart, serial->rx_ring.buf, serial->rx_ring.buf_size);
}

void dma_serial_write(struct dma_serial *serial, uint8_t data){
	dma_tx_putc(&serial->tx_info, data);
}

void dma_serial_puts(struct dma_serial *serial, uint8_t *data, int length){
	dma_tx_puts(&serial->tx_info, data, length);
}

void dma_serial_flush(struct dma_serial *serial){
	dma_tx_flush(&serial->tx_info);
}

uint8_t dma_serial_read(struct dma_serial *serial){
	uint8_t c;
	dma_ring_getc(&serial->rx_ring, &c);
	return c;
}

int dma_serial_available(struct dma_serial *serial){
	return dma_ring_available(&serial->rx_ring);
}

void dma_seial_clear(struct dma_serial *serial){
	while(dma_ring_available(&serial->rx_ring) > 0){
		(void)dma_ring_getc(&serial->rx_ring, (uint8_t *)NULL);
	}
}

void dma_serial_send_it(struct dma_serial *serial, UART_HandleTypeDef *huart){
	dma_tx_send_it(&serial->tx_info, huart);
}
