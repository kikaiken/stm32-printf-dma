//
// Created by Akitomo Kuraku on 25/02/07.
//

#ifndef HAL_PRINTF_TX_DMA_TX_H
#define HAL_PRINTF_TX_DMA_TX_H
#include "dma_ring.h"
#include "main.h"

struct dma_tx_info{
	struct dma_ring_buf tx_ring;
	int sending;
	UART_HandleTypeDef *huart;
	int previous_send_len;
};

void dma_tx_init(struct dma_tx_info *tx, UART_HandleTypeDef *tx_huart);
void dma_tx_putc(struct dma_tx_info *tx, uint8_t c);
void dma_tx_puts(struct dma_tx_info *tx, uint8_t *data, size_t data_len);
void dma_tx_send_it(struct dma_tx_info *tx, UART_HandleTypeDef *tx_huart);
void dma_tx_flush(struct dma_tx_info *tx);
#endif //HAL_PRINTF_TX_DMA_TX_H
