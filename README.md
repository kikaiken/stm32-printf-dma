# STM32-printf-DMA
UARTをDMAでprintfしたいためのライブラリ。
### UART
Arduino界隈ではシリアルと呼ばれているものです。
### DMA
CPUを介さずレジスタからバスを通して直接UART機能にデータが送られるようになります。
割り込みやポーリングと違って、処理が滞る心配がありません。
### printf
組み込みでは標準ではないprintfですが、変数と文字の混合表示や小数の値表示の指定などが便利ですので使いたいときがあります。

# 注意
scanfはテスト不足のため事前に動作を確かめてください。

# 使い方

## ソース・ヘッダの配置
Incディレクトリにヘッダファイルを、Srcディレクトリにソースをコピーしてください。
submoduleする場合はプロジェクトの設定でインクルードパスの指定などが必要と思われます。
実際、CubeIDE1.1.0ではsubmoduleで使えないときがあります。

## CubeMX DMAの設定
UART自体の設定には特に依存しません。
printfを使用したいUARTのポート設定でDMAタブについて、RXはDMA Circular、TXはDMA Normalに設定し、global interruptを有効にする

## インクルード
以下のヘッダーファイルをインクルード
main.c内の以下の場所に記述してください。
注：コメントはCubeMXですでに生成されているものです。
```c:main.c
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>

#include "dma_printf.h"
#include "dma_scanf.h"

/* USER CODE END Includes */
```


## 割込み設定
TX側はDMAの送信完了割込みを利用するため、割り込みハンドラを設定しておく。
開発者側はmain.c内においてますが、stm32fxxx_it.c内の方が正しいのかもしれません（未検証）
場所は指定しません。
```c:main.c
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
      dma_printf_send_it(huart);
}
```

## printf/scanf関数の有効化
以下のコードによって、DMAを利用したprintf/scanfが有効となる。
このコードはmain.c内であれば場所は指定しません。
```c:main.c
int __io_putchar(int ch)
{
      dma_printf_putc(ch&0xFF);
        return ch;
}

int __io_getchar(void)
{
      return dma_scanf_getc_blocking();
}
```

## 関数初期化
huart2を利用する場合は、main関数内でDMA,UART(USART)の初期化後、以下の場所に初期化する。
注：これより前ではprintf,scanfは使用できません。
```c:main.c
  /* USER CODE BEGIN 2 */
  setbuf(stdin, NULL);
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);
  dma_printf_init(&huart2);   //printfを使いたいUARTポートの構造体のポインタ
  dma_scanf_init(&huart2);    //scanfを使いたいUARTポートの構造体のポインタ

  //.........
  /* USER CODE END 2 */
```
## float小数を表示させたいとき(オプション)
プロジェクトのプロパティから
C/C++Build->preference
toolsタブ->MCU GCC Linker->others->other flags
で `-u _printf_float` を追加します。


# 観測されている問題
- scanfはテスト不足のため動作未確認です。
- submoduleができないときがあります。

# `dma_serial.h`
ArduinoのSerialクラスに模した動作をするものです。
UARTチャンネル毎に`struct dma_serial`型の構造体変数を宣言して使ってください。
関数についてはコードを見てください。

# 上級者向け
`dma_tx.h`はdma_printf.hの機能を、`dma_rx.h`はdma_scanf.hの機能を複数のUARTにて使うためのライブラリです。
UARTごとに`struct dma_tx_info`, `struct dma_rx_info`型の構造体変数を宣言して使ってください。
詳細はコードを見てください。（要追記）

# 更新履歴
- 2019/09/16 公開
- 2019/11/18 CNDTR-NDTR問題をマクロ使用により解決,submodule不可を観測
- 2025/02/07 変数・関数型を微修正
- 2025/02/09 `dma_tx.h`, `dma_rx.h`を追加
- 2025/02/13 `dma_serial.h`を追加
