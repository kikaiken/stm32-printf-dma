//
// Created by Akitomo Kuraku on 25/02/13.
//

#ifndef HAL_DMA_PRINTF_DMA_SERIAL_H
#define HAL_DMA_PRINTF_DMA_SERIAL_H
#include "dma_ring.h"
#include "dma_tx.h"
#include "main.h"


struct dma_serial{
	struct dma_tx_info tx_info;
	struct dma_ring_buf rx_ring;
	UART_HandleTypeDef *huart;
};

void dma_serial_init(struct dma_serial *serial, UART_HandleTypeDef *huart);

void dma_serial_write(struct dma_serial *serial, uint8_t data);
void dma_serial_puts(struct dma_serial *serial, uint8_t *data, int length);
void dma_serial_flush(struct dma_serial *serial);

uint8_t dma_serial_read(struct dma_serial *serial);
int dma_serial_available(struct dma_serial *serial);
void dma_seial_clear(struct dma_serial *serial);

void dma_serial_send_it(struct dma_serial *serial, UART_HandleTypeDef *huart);

#endif //HAL_DMA_PRINTF_DMA_SERIAL_H
